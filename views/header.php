<?php
if (isset($_SESSION['id']) &&!isset($soldier)){
    $pdo = get_pdo();
    $soldiers = new Calendar\Soldiers($pdo);
    $soldier = $soldiers->find($_SESSION['id']);

}
?>
<!Doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="./js/script.js"></script>
    <link rel="stylesheet" href="css/calendar.css">
    <link rel="stylesheet" href="css/index.css">
  <title><?= isset($title) ? h($title) : 'Mon calendrier'; ?></title>
</head>
<body>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <h1 class="navbar-brand">Gestion des permissions</h1>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link" href="index.php">Accueil</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="calendar.php">Vue calendrier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="leaves.php">Vue permissions</a>
        </li>
        <?php if(isset($_SESSION['id'])){ ?>
        <li class="nav-item">
            <a class="nav-link" href="logout.php">Se déconnecter (<?=$soldier->getFirstName().' ' .$soldier->getLastName()?>)</a>
        </li>
        <?php } ?>
    </ul>
</nav>