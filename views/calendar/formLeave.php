<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <input id="name" type="hidden" class="form-control" name="name" value="<?= isset($data['name']) ? h($data['name']) : 'permission'; ?>">
            <?php /*if (isset($errors['name'])): ?>
                <small class="form-text text-muted"><?= $errors['name']; ?></small>
            <?php endif;  */?>
            <label for="startDate">Date début</label>
            <input id="startDate" type="date" required class="form-control" name="startDate" value="<?= isset($data['startDate']) ? h($data['startDate']) : ''; ?>">
            <?php if (isset($errors['startDate'])): ?>
                <small class="form-text text-muted"><?= $errors['startDate']; ?></small>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <fieldset class="form-group">
                <label for="start">Heure de début</label>
                <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="start" id="start" value="06:30" <?= isset($data['start'])&& $data['start']!="12:00" ? "checked" : ''; //chgt condition par defaut, on choisi le matin?>> 
                    Matin
                </label>
                </div>
                <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="start" id="start" value="12:00" <?= isset($data['start'])&& $data['start']=="12:00" ? "checked" : ''; ?>>
                    Midi
                </label>
                </div>
            </fieldset>
            
            <?php if (isset($errors['start'])): ?>
                <small class="form-text text-muted"><?= $errors['start']; ?></small>
            <?php endif; ?>
        </div>
    </div>
    
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="endDate">Date de fin</label>
            <input id="endDate" type="date" required class="form-control" name="endDate" value="<?= isset($data['endDate']) ? h($data['endDate']) : ''; ?>">
            <?php if (isset($errors['endDate'])): ?>
                <small class="form-text text-muted"><?= $errors['endDate']; ?></small>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
        <fieldset class="form-group">
            <label for="end">Heure de fin</label>
                <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="end" id="end" value="11:00" <?= isset($data['end'])&& $data['end']=="11:00" ? "checked" : ''; ?>>
                    Midi
                </label>
                </div>
                <div class="form-check">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="end" id="end" value="16:10" <?= isset($data['end'])&& $data['end']!="11:00" ? "checked" : ''; //chgt condition par defaut, on choisi le soir?>>
                    Soir
                </label>
                </div>
            </fieldset>
        </div>
    </div>
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" required id="description" class="form-control"><?= isset($data['description']) ? h($data['description']) : ''; ?></textarea>
</div>

<div class="form-group">
    <label for="type">Type</label>
    <select class="form-control" id="type" name="type">
    <?php
    foreach (getLeavesInfos() as $key => $value) {
        if(isset($data["type"]) && $key===$data["type"])
            echo '<option value="'.$key.'" selected>'.$value[1].'</option>';
        else
            echo '<option value="'.$key.'">'.$value[1].'</option>';
    }
    ?>
    </select>
</div>
<div class="form-group">
    <input id="state" type="hidden" class="form-control" name="state" value="posee">
</div>
<div class="form-group">
    <label for="location">Adresse de la permission</label>
    <textarea name="location" id="location" class="form-control"><?= isset($data['location']) ? h($data['location']) : ''; ?></textarea>
</div>
<div class="form-group">
    <label for="phone">Téléphone</label>
    <input id="phone" type="text" required class="form-control" name="phone" value="<?= isset($data['phone']) ? h($data['phone']) : ''; ?>">
    <?php if (isset($errors['phone'])): ?>
        <small class="form-text text-muted"><?= $errors['phone']; ?></small>
    <?php endif; ?>
</div>
<input id="soldier" type="hidden" class="form-control" name="soldier" value="<?=$_SESSION['id']?>">


