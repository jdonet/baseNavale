-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Dim 02 Décembre 2018 à 18:04
-- Version du serveur :  5.7.23-0ubuntu0.18.04.1
-- Version de PHP :  7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `calendrier`
--

-- --------------------------------------------------------

--
-- Structure de la table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `company`
--

INSERT INTO `company` (`id`, `name`) VALUES
(1, 'Première compagnie'),
(2, 'Deuxième compagnie');

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `start`, `end`) VALUES
(1, 'QuÃ©bec', 'Voyage exploratoire', '2018-08-09 21:48:00', '2018-08-12 21:48:00'),
(2, 'test nom bisff', 'test desc bis', '2018-08-16 13:02:00', '2018-08-16 13:02:00'),
(3, 'Permission modifiÃ©e', 'test description', '2018-09-12 14:28:00', '2018-09-12 14:28:00'),
(4, 'permission', 'Gastro', '2018-08-30 20:42:00', '2018-09-02 20:42:00'),
(5, 'dsds', '', '2018-09-01 13:03:00', '2018-09-01 13:03:00'),
(6, 'permission', 'Test Permission', '2018-11-15 06:30:00', '2018-11-24 16:10:00'),
(7, 'permission', 'cinema avec ma femme', '2018-11-18 12:00:00', '2018-11-20 16:10:00'),
(8, 'garde', NULL, '2018-11-30 00:00:00', '2018-11-30 00:00:00'),
(9, 'garde', NULL, '2018-12-01 00:00:00', '2018-12-01 00:00:00'),
(10, 'garde', NULL, '2018-12-04 00:00:00', '2018-12-04 00:00:00'),
(11, 'garde', NULL, '2018-11-08 00:00:00', '2018-11-08 00:00:00'),
(12, 'permission', 'dsds', '2018-10-18 06:30:00', '2018-10-18 16:10:00');

-- --------------------------------------------------------

--
-- Structure de la table `guard`
--

CREATE TABLE `guard` (
  `id` int(11) NOT NULL,
  `refGuardType` varchar(20) NOT NULL,
  `refSoldier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `guard`
--

INSERT INTO `guard` (`id`, `refGuardType`, `refSoldier`) VALUES
(8, 'OPSC', 1),
(9, 'OPSC', 2),
(10, 'OPSC', 1),
(11, 'OPSC', 2);

-- --------------------------------------------------------

--
-- Structure de la table `guardType`
--

CREATE TABLE `guardType` (
  `id` varchar(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `guardType`
--

INSERT INTO `guardType` (`id`, `name`) VALUES
('CGA', 'Chef groupe d\'attaque'),
('Cuisine', 'Suisinier'),
('EGA1', 'Equipe de groupe d\'attaque 1'),
('EGA2', 'Equipe de groupe d\'attaque 2'),
('OG', 'Officier de garde'),
('OPES', 'Officier protection et soutiens'),
('OPSC', 'Officier protection du service courant'),
('VPI', 'Véhicule protection incendie');

-- --------------------------------------------------------

--
-- Structure de la table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `state` set('posee','visa','validee','refusee') NOT NULL DEFAULT '',
  `location` text NOT NULL,
  `phone` varchar(30) NOT NULL,
  `refSoldier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `leaves`
--

INSERT INTO `leaves` (`id`, `type`, `state`, `location`, `phone`, `refSoldier`) VALUES
(1, 'HT', 'posee', 'MontrÃ©al\r\nSherbrooke', '87257935', 1),
(2, 'HT', 'posee', 'adressesdsdssd', '123434212331', 1),
(3, 'HT', 'posee', 'Pirae Arue', '+689 87257935', 1),
(4, 'STAM', 'posee', 'A domicile', '1 888-327-2823', 1),
(6, 'STRec', 'visa', 'df', '123', 1),
(7, 'STAb', 'refusee', '', '123', 2),
(12, 'STAM', 'posee', 'sds', 'dsds', 2);

-- --------------------------------------------------------

--
-- Structure de la table `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `refSector` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `section`
--

INSERT INTO `section` (`id`, `name`, `refSector`) VALUES
(1, 'Injections', 3),
(2, 'Hors Bord', 3),
(3, 'Auxiliaires', 4),
(4, 'Hydro Froid', 4),
(5, 'Armes', 6),
(6, 'SIC', 6),
(7, 'Detection', 6),
(8, 'Instrumentation', 7),
(9, 'Navigation', 7),
(10, 'Manœuvre', 16),
(11, 'Antipollution', 16);

-- --------------------------------------------------------

--
-- Structure de la table `sector`
--

CREATE TABLE `sector` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `refService` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `sector`
--

INSERT INTO `sector` (`id`, `name`, `refService`) VALUES
(1, 'Moteur', 1),
(2, 'Drome', 1),
(3, 'Injection / Hors.bord', 1),
(4, 'Auxiliaires Hydro', 1),
(5, 'Chaudronnerie', 1),
(6, 'Armes Equipement', 2),
(7, 'Electricité', 2),
(8, 'Financement', 3),
(9, 'Magasinage Navale', 3),
(10, 'Materiel', 3),
(11, 'Flux Transit', 3),
(12, 'Magasin', 4),
(13, 'Manutention', 4),
(14, 'Vie Courante', 8),
(15, 'Bureau Sport', 8),
(16, 'Manoeuvre / Antipollution', 9),
(17, 'Combustibles / Technique', 9),
(18, 'Plongée', 9),
(19, 'Matériel', 11),
(20, 'Entretien / formation', 11),
(21, 'Secretariat Général', 12),
(22, 'Bureau RH', 12),
(23, 'Bureau SIC', 12);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `refCompany` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `service`
--

INSERT INTO `service` (`id`, `name`, `refCompany`) VALUES
(1, 'Flotteur Mobilité', 2),
(2, 'ARMEQ/ELEC', 2),
(3, 'Logistique Navale', 2),
(4, 'Moyen terrestres', 2),
(5, 'CMO', 2),
(6, 'INFRA', 2),
(7, 'Commandement', 2),
(8, 'Service Général', 1),
(9, 'Accueil Portuaire', 1),
(10, 'Dock', 1),
(11, 'Sécurité', 1),
(12, 'AG', 1),
(13, 'CIRFA', 1),
(14, 'SLPA', 1);

-- --------------------------------------------------------

--
-- Structure de la table `soldier`
--

CREATE TABLE `soldier` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `regimentalNumber` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `refCompany` int(11) NOT NULL,
  `refSector` int(11) DEFAULT NULL,
  `refService` int(11) DEFAULT NULL,
  `refSection` int(11) DEFAULT NULL,
  `refGuardType` varchar(20) DEFAULT NULL,
  `guardTypeLeader` tinyint(1) NOT NULL,
  `sectorLeader` tinyint(1) NOT NULL DEFAULT '0',
  `serviceLeader` tinyint(1) NOT NULL DEFAULT '0',
  `companyLeader` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `soldier`
--

INSERT INTO `soldier` (`id`, `firstname`, `lastname`, `regimentalNumber`, `login`, `password`, `refCompany`, `refSector`, `refService`, `refSection`, `refGuardType`, `guardTypeLeader`, `sectorLeader`, `serviceLeader`, `companyLeader`) VALUES
(1, 'Matthieu', 'Ferron', 235, 'matthieu', '554e01a08b1101466c834219f8604574', 2, 6, 2, 6, 'OPSC', 0, 0, 0, 0),
(2, 'Julien', 'Donet', 55, 'julien', '1d183da10b2db09a8691d1db5e23ad07', 2, 6, 2, 7, 'OPSC', 0, 1, 0, 0),
(3, 'Teihotu', 'Chavez', 235, 'teihotu', '082dd3179733e3e716a58eb90f418a78', 2, 6, 2, 7, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `visa`
--

CREATE TABLE `visa` (
  `refLeave` int(11) NOT NULL,
  `refSoldier` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `accepte` tinyint(1) NOT NULL,
  `motifRefus` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `guard`
--
ALTER TABLE `guard`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refguardType` (`refGuardType`),
  ADD KEY `refSoldier` (`refSoldier`);

--
-- Index pour la table `guardType`
--
ALTER TABLE `guardType`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refSoldier` (`refSoldier`);

--
-- Index pour la table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refSecteur` (`refSector`);

--
-- Index pour la table `sector`
--
ALTER TABLE `sector`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refService` (`refService`);

--
-- Index pour la table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refCompany` (`refCompany`);

--
-- Index pour la table `soldier`
--
ALTER TABLE `soldier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD KEY `refSector` (`refSector`),
  ADD KEY `refService` (`refService`),
  ADD KEY `refSection` (`refSection`),
  ADD KEY `refCompany` (`refCompany`),
  ADD KEY `refGuardType` (`refGuardType`);

--
-- Index pour la table `visa`
--
ALTER TABLE `visa`
  ADD PRIMARY KEY (`refLeave`,`refSoldier`),
  ADD KEY `refSoldier` (`refSoldier`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `guard`
--
ALTER TABLE `guard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `sector`
--
ALTER TABLE `sector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `soldier`
--
ALTER TABLE `soldier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `guard`
--
ALTER TABLE `guard`
  ADD CONSTRAINT `guard_ibfk_1` FOREIGN KEY (`refGuardType`) REFERENCES `guardType` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guard_ibfk_2` FOREIGN KEY (`refSoldier`) REFERENCES `soldier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `guard_ibfk_3` FOREIGN KEY (`id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `leaves`
--
ALTER TABLE `leaves`
  ADD CONSTRAINT `leaves_ibfk_1` FOREIGN KEY (`id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `leaves_ibfk_2` FOREIGN KEY (`refSoldier`) REFERENCES `soldier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`refSector`) REFERENCES `sector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sector`
--
ALTER TABLE `sector`
  ADD CONSTRAINT `sector_ibfk_1` FOREIGN KEY (`refService`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`refCompany`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `soldier`
--
ALTER TABLE `soldier`
  ADD CONSTRAINT `soldier_ibfk_1` FOREIGN KEY (`refSector`) REFERENCES `sector` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soldier_ibfk_2` FOREIGN KEY (`refService`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soldier_ibfk_3` FOREIGN KEY (`refSection`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soldier_ibfk_4` FOREIGN KEY (`refCompany`) REFERENCES `company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soldier_ibfk_5` FOREIGN KEY (`refGuardType`) REFERENCES `guardType` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `visa`
--
ALTER TABLE `visa`
  ADD CONSTRAINT `visa_ibfk_1` FOREIGN KEY (`refLeave`) REFERENCES `leaves` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `visa_ibfk_2` FOREIGN KEY (`refSoldier`) REFERENCES `soldier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;