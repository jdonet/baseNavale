<?php
require '../vendor/autoload.php';
//demarrage session
if (session_status() == PHP_SESSION_NONE) {
  session_start();
}

function e404 () {
    require '../public/404.php';
    exit();
}

function dd(...$vars) {
    foreach($vars as $var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }
}

function get_pdo (): PDO {
    return new PDO('mysql:host=localhost;dbname=calendrier', 'root', 'adastag_5', [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ]);
}

function h(string $value): string {
    if ($value === null) {
        return '';
    }
    return htmlentities($value);
}

function render(string $view, $parameters = []) {
    extract($parameters);
    include "../views/{$view}.php";
}

function getLeavesInfos(){
    return [
        "Garde" => ["bg-danger text-white","Garde"],
        "Perm" => ["bg-primary text-white","Permission"],
        "HT" => ["bg-success text-white","Permission hors territoire"],
        "STAM" => ["bg-info text-white","Arrêt maladie"],
        "STAb" => ["bg-warning text-white","Absence"],
        "STEM" => ["bg-danger text-white","Garde enfant malade"],
        "STRec" => ["bg-inverse text-white","Récupération de garde"],
        "STAu" => ["bg-faded","Autres"],
    ];
}

function getLeavesStates(){ ///Attention, l'ordre est important, car je me sers du premier pour désactiver la modification
    return [
        "posee" => ["text-muted"],
        "visa" => ["text-warning"],
        "validee" => ["text-success"],
        "refusee" => ["text-danger s"],
    ];
}
