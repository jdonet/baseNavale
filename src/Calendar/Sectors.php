<?php
namespace Calendar;

class Sectors {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une section
     * @param int $id
     * @return Sector
     * @throws \Exception
     */
    public function find (int $id): Sector {
        $statement = $this->pdo->query("SELECT * FROM sector WHERE sector.id = $id LIMIT 1");
        if($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $sector = new Sector($row['name']);
            //positionnement de l'id
            $sector->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $sector;
    }

   
}
