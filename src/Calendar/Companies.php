<?php
namespace Calendar;

class Companies {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une compagnie
     * @param int $id
     * @return Company
     * @throws \Exception
     */
    public function find (int $id): Company {
        $statement = $this->pdo->query("SELECT * FROM company WHERE company.id = $id LIMIT 1");
        if($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $company = new Company($row['name']);
            //positionnement de l'id
            $company->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $company;
    }

   
}
