<?php
namespace Calendar;

class GuardType{

    private $id;
    private $libelle;

    public function __construct($libelle =""){
        $this->libelle = $libelle;
    }

    public function getId(): string {
        return $this->id;
    }

    public function getLibelle(): string {
        return $this->libelle;
    }

    public function setId (string $id) {
        $this->id = $id;
    }

    public function setLibelle (string $libelle) {
        $this->libelle = $libelle;
    }
}
