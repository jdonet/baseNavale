<?php
namespace Calendar;

class Soldier{

    private $id;
    private $firstname;
    private $lastname;
    private $regimentalNumber;
    private $company;
    private $companyLeader;
    private $sector;
    private $sectorLeader;
    private $service;
    private $serviceLeader;
    private $section;
    private $guardType; //le type de garde auquel on appartient
    private $guardTypeLeader;
    private $guardTypeLeaderObj; //le type de garde dont on est chef
    private $login;
    private $password;

    public function __construct($firstname ="", $lastname="",  $regimentalNumber="", $login="", $password="",$company="",$companyLeader=false, $sector="",$sectorLeader=false, $service="",$serviceLeader=false, $section="",$guardType="",$guardTypeLeader=false,$guardTypeLeaderObj=""){
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->regimentalNumber = $regimentalNumber;
        $this->login = $login;
        $this->password = $password;
        $this->company = $company;
        $this->companyLeader = ($companyLeader === '1');
        $this->sector = $sector;
        $this->sectorLeader =  ($sectorLeader === '1');
        $this->service = $service;
        $this->serviceLeader = ($serviceLeader === '1');
        $this->section = $section;
        $this->guardTypeLeader = ($guardTypeLeader === '1');
        $this->guardType = $guardType;
        $this->guardTypeLeaderObj = $guardTypeLeaderObj;
    
    }

    public function getId(): int {
        return $this->id;
    }

    public function getFirstName(): string {
        return $this->firstname;
    }

    public function getLastName (): string {
        return $this->lastname;
    }

    public function getRegimentalNumber (): int {
        return $this->regimentalNumber;
    }

    public function getLogin (): string {
        return $this->login;
    }

    public function getPassword (): string {
        return $this->password;
    }

    public function getCompany (): Company {
        return $this->company;
    }

    public function isCompanyLeader (): bool {
        return $this->companyLeader;
    }

    public function getSector () {
        return $this->sector;
    }

    public function isSectorLeader (): bool {
        return $this->sectorLeader;
    }

    public function getService () {
        return $this->service;
    }

    public function isServiceLeader (): bool {
        return $this->serviceLeader;
    }

    public function getSection () {
        return $this->section;
    }

    public function getGuardType (){
        return $this->guardType;
    }

    public function isGuardTypeLeader (): bool {
        return $this->guardTypeLeader;
    }

    public function getGuardTypeLeaderObj (){
        return $this->guardTypeLeaderObj;
    }
    
    public function setId (int $id) {
        $this->id = $id;
    }

    public function setFirstName (string $firstname) {
        $this->firstname = $firstname;
    }

    public function setLastName (string $lastname) {
        $this->lastname = $lastname;
    }

    public function setRegimentalNumber (string $regimentalNumber) {
        $this->regimentalNumber = $regimentalNumber;
    }

    public function setLogin (string $login) {
        $this->login = $login;
    }

    public function setPassword (string $password) {
        $this->password = $password;
    }

    public function setCompany (Company $company) {
        $this->company = $company;
    }

    public function setCompanyLeader (boolean $companyLeader) {
        $this->companyLeader = $companyLeader;
    }

    public function setSector (Sector $sector) {
        $this->sector = $sector;
    }
    
    public function setSectorLeader (boolean $sectorLeader) {
        $this->sectorLeader = $sectorLeader;
    }

    public function setService (Service $service) {
        $this->service = $service;
    }

    public function setServiceLeader (boolean $serviceLeader) {
        $this->serviceLeader = $serviceLeader;
    }

    public function setSection (Section $section) {
        $this->section = $section;
    }

    public function setGuardType (GuardType $gt) {
        $this->guardType = $gt;
    }

    public function setGuardTypeLeader (boolean $gtl) {
        $this->guardTypeLeader = $gtl;
    }

    public function setGuardTypeLeaderObj (GuardType $gt) {
        $this->guardTypeLeaderObj = $gt;
    }
    
}
