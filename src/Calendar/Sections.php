<?php
namespace Calendar;

class Sections {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une section
     * @param int $id
     * @return Section
     * @throws \Exception
     */
    public function find (int $id): Section {
        $statement = $this->pdo->query("SELECT * FROM section WHERE section.id = $id LIMIT 1");
        if($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $section = new Section($row['name']);
            //positionnement de l'id
            $section->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $section;
    }

   
}
