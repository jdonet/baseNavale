<?php
namespace Calendar;

class Soldiers {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère un soldat
     * @param int $id
     * @return Soldier
     * @throws \Exception
     */
    public function find (int $id): Soldier {
        $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.id = $id LIMIT 1");
        if($row = $statement->fetch()) {
            $section=NULL;
            $service=NULL;
            $sector=NULL;
            $company=NULL;
            $guardType=NULL;
            $guardTypeLeaderObj=NULL;

            //appel du constructeur paramétré
            $soldier = new Soldier($row['firstname'],$row['lastname'],$row['regimentalNumber'],$row['login'],$row['password'],$company,$row['companyLeader'],$sector,$row['sectorLeader'],$service,$row['serviceLeader'],$section,$guardType,$row['guardTypeLeader'],$guardTypeLeaderObj);            //positionnement de l'id

            //récupération de la compagnie du soldat
            if(!is_null($row["refCompany"])) {
                $companies = new \Calendar\Companies($this->pdo);
                $company = $companies->find($row['refCompany']);
                $soldier->setCompany($company);
            }    

            //récupération de la section du soldat
            if(!is_null($row["refSection"])) {
                $sections = new \Calendar\Sections($this->pdo);
                $section = $sections->find($row['refSection']);
                $soldier->setSection($section);
            }

            //récupération du service du soldat
            if(!is_null($row["refService"])) {
                $services = new \Calendar\Services($this->pdo);
                $service = $services->find($row['refService']);        
                $soldier->setService($service);
            }

            //récupération du secteur du soldat
            if(!is_null($row["refSector"])) {
                $sectors = new \Calendar\Sectors($this->pdo);
                $sector = $sectors->find($row['refSector']);
                $soldier->setSector($sector);
            }

            //récupération du type de garde du soldat
            if(!is_null($row["refGuardType"])) {
                $gts = new \Calendar\GuardTypes($this->pdo);
                $gt = $gts->find($row['refGuardType']);
                $soldier->setGuardType($gt);
            }

            //récupération du type de garde du soldat dont il est chef
            if(!is_null($row["refGuardTypeLeader"])) {
                $gts = new \Calendar\GuardTypes($this->pdo);
                $gt = $gts->find($row['refGuardTypeLeader']);
                $soldier->setGuardTypeLeaderObj($gt);
            }

            $soldier->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $soldier;
    }

    /**
     * Récupère la liste des soldats dont il peut consulter le planning de permissions
     * @param int id
     * @return Soldier[]
     * @throws \Exception
     */
    public function underOrders(int $id): array {
        $soldier = $this->find($id);     
        
        $company=$soldier->getCompany()!=null?$soldier->getCompany()->getId():"NULL";
        $service=$soldier->getService()!=null?$soldier->getService()->getId():"NULL";
        $sector=$soldier->getSector()!=null?$soldier->getSector()->getId():"NULL";
        $section=$soldier->getSection()!=null?$soldier->getSection()->getId():"NULL";

        $soldiers = array();
        if($soldier->isCompanyLeader()){
            $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.refCompany = ".$company ." AND soldier.id != $id ORDER by lastname,firstname");
        }else if($soldier->isServiceLeader()){
            $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.refService = ".$service ." AND soldier.id != $id ORDER by lastname,firstname");
        }else if($soldier->isSectorLeader()){
            $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.refSector = ".$sector." AND soldier.id != $id ORDER by lastname,firstname");
        }else { //chef de rien
            $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.refSection = ".$section." AND soldier.refSector = ".$sector." AND soldier.refService = ".$service." AND soldier.refCompany = ".$company." AND soldier.id != $id ORDER by lastname,firstname");
        }
        
        while($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $soldier = $this->find($row['id']);
            $soldiers[] = $soldier;
        }
        return $soldiers;
    }

    /**
     * Récupère la liste des soldats dont il peut consulter le planning de garde
     * @param int id
     * @return Soldier[]
     * @throws \Exception
     */
    public function onSameGuardType(int $id): array {
        $soldier = $this->find($id); 
        $soldiers = array();
    
        if (null !== $soldier->getGuardType()){
            $guardTypeId = $soldier->getGuardType()->getId();
            $statement = $this->pdo->query("SELECT * FROM soldier WHERE soldier.refGuardType = '".$guardTypeId."' ORDER by lastname,firstname");
            
            while($row = $statement->fetch()) {
                //appel du constructeur paramétré
                $soldier = $this->find($row['id']);
                $soldiers[] = $soldier;
            }
        }
        return $soldiers;
    }

/**
     * Vérifie la connexion d'un soldat. Renvoie false si erreur de cnx
     * @param int $id
     * @return Soldier | boolean
     */
    public function connect (string $login,string $pass) {
        $statement = $this->pdo->prepare("SELECT * FROM soldier WHERE soldier.login = ? and soldier.password=md5(?) LIMIT 1");
        $statement->bindparam(1,$login);
        $statement->bindparam(2,$pass);
        $statement->execute();
        if($row = $statement->fetch()) {
            $soldier = $this->find($row['id']);
        }

        if ($row === false) {
            return false;
        }
        return $soldier;
    }

    /**
     * Récupère la liste des permissions à viser
     * @param int id du soldat qui peut viser une garde
     * @return Leave[]
     * @throws \Exception
     */
    public function getLeavesToCheck(int $id, $leaves): array {
        $soldier = $this->find($id); 
        $leavesToCheck = array();
    
        if ( !$soldier->isGuardTypeLeader() && !$soldier->isServiceLeader() && !$soldier->isSectorLeader() && !$soldier->isCompanyLeader()  ) //s'il est chef de rien
            return $leavesToCheck; //rien à viser
        else{
            
            foreach ($leaves as $leave) {
                $this->canCheck($leave,$id) ? $leavesToCheck[]=$leave:null;
            }
            return $leavesToCheck;
        }
    }
    
/**
     * Récupère la liste des permissions à viser
     * @param int id du soldat qui peut viser une garde
     * @return Leave[]
     * @throws \Exception
     */
    public function canCheck(Leave $leave, int $idSoldier): bool {
        $soldier = $this->find($idSoldier); 

    
        if ( !$soldier->isGuardTypeLeader() && !$soldier->isServiceLeader() && !$soldier->isSectorLeader() && !$soldier->isCompanyLeader()  ) //s'il est chef de rien
            return false; //rien à viser
        else{
            if($leave->getState() != "validee" && $leave->getState() != "supprimee"){ //on étudie que celles posées et en visa
                //avoir le nb visa deja fait
                $leavesMng = new \Calendar\Leaves($this->pdo);
                $nbVisa = $leavesMng->getNbVisa($leave);
                switch ($nbVisa) {
                    case 0: //aucun visa encore / chef de garde
                        if($soldier->isGuardTypeLeader() && $leave->getSoldier()->getGuardType() ==  $soldier->getGuardTypeLeaderObj())
                        return true;
                        break;
                    case 1: //1 visa / chef de secteur
                        if($soldier->isSectorLeader() && $leave->getSoldier()->getSector() ==  $soldier->getSector())
                        return true;
                        break;
                    case 2: //2 visa / chef de service
                        if($soldier->isServiceLeader() && $leave->getSoldier()->getService() ==  $soldier->getService())
                        return true;
                        break;
                    case 3: //3 visa / chef de compagnie
                        if($soldier->isCompanyLeader() && $leave->getSoldier()->getCompany() ==  $soldier->getCompany())
                        return true;
                        break;
                }
            }
            
            
        }
        return false;
    }
    /**
     * //////////////////////////////////jamais utilisée ??? Pas sur du tout des champs sector/section ...
     * @param Soldier $soldier
     * @param array $data
     * @return Soldier
     */
  /*  public function hydrate (Soldier $soldier, array $data) {
        $soldier->setFirstName($data['firstname']);
        $soldier->setLastName($data['lastname']);
        $soldier->setRegimentalNumber($data['regimentalNumber']);
        $soldier->setLogin($data['login']);
        $soldier->setPassword($data['password']);
        $companies = new \Calendar\Companies($this->pdo);
        $company = $companies->find($data['company']);
        $soldier->setCompany($company);
        $sectors = new \Calendar\Sectors($this->pdo);
        $sector = $sectors->find($data['sector']);
        $soldier->setSector($sector);
        $services = new \Calendar\Services($this->pdo);
        $service = $services->find($data['service']);        
        $soldier->setService($service);
        $sections = new \Calendar\Sections($this->pdo);
        $section = $sections->find($data['section']);
        $soldier->setSection($section);

        return $soldier;
    }
*/

}
