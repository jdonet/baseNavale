<?php
namespace Calendar;

class Leave extends Event{

    private $type;
    private $state;
    private $location;
    private $phone;
    private $soldier;

    public function __construct($name ="", $description="", $start="", $end="", $type="", $state="", $location="", $phone="",$soldier=""){
        parent::__construct($name, $description, $start, $end);
        $this->type = $type;
        $this->state = $state;
        $this->location = $location;
        $this->phone = $phone;
        $this->soldier = $soldier;
    }

    public function getType (): string {
        return $this->type;
    }

    public function getState (): string {
        return $this->state;
    }

    public function getLocation (): string {
        return $this->location;
    }

    public function getPhone (): string {
        return $this->phone;
    }

    public function getSoldier (): Soldier {
        return $this->soldier;
    }

    public function setType (string $type) {
        $this->type = $type;
    }

    public function setState (string $state) {
        $this->state = $state;
    }

    public function setLocation (string $location) {
        $this->location = $location;
    }

    public function setPhone (string $phone) {
        $this->phone = $phone;
    }

    public function setSoldier (Soldier $soldier) {
        $this->soldier = $soldier;
    }
}
