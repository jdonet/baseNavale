<?php
namespace Calendar;

class Leaves {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

/**
     * Récupère les permissions a viser
     * @return Leave[]
     */
    public function getAllLeavesToCheck (): array {
        $sql = "SELECT * FROM leaves l ,events e WHERE e.id=l.id AND (state='posee' OR state='visa') ORDER BY start ASC";
        $statement = $this->pdo->query($sql);
        $leaves=[];
        while ($result = $statement->fetch()) {
            $leaves[] = $this->find($result['id']);
        }
        return $leaves;
    }

    

    /**
     * Récupère les permissions commençant entre 2 dates pour un soldat
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return Leave[]
     */
    public function getLeavesBetween (\DateTimeInterface $start, \DateTimeInterface $end,int $idSoldier ): array {
        $sql = "SELECT * FROM leaves l ,events e WHERE refSoldier =$idSoldier AND e.id=l.id AND (start BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}' OR end BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}') ORDER BY start ASC";
        $statement = $this->pdo->query($sql);
        $leaves=[];
        while ($result = $statement->fetch()) {
            $leaves[] = $this->find($result['id']);
        }
        return $leaves;
    }

    /**
     * Récupère les permissions commençant entre 2 dates indexé par jour pour un soldat
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return array
     */
    public function getLeavesBetweenByDay (\DateTimeInterface $start, \DateTimeInterface $end,int $idSoldier): array {
        $leaves = $this->getLeavesBetween($start, $end,$idSoldier);
        $days = [];
        foreach($leaves as $leave) {
            $startDate = $leave->getStart()->format('Y-m-d');
            $endDate = $leave->getEnd()->format('Y-m-d');
            if($startDate != $endDate){
                $nextDay = $startDate;
                $nextEndDate = date('Y-m-d', strtotime('+1 day', strtotime($endDate)));
                while($nextDay!= $nextEndDate){
                    if (!isset($days[$nextDay])) {
                        $days[$nextDay] = [$leave];
                    } else {
                        $days[$nextDay][] = $leave;
                    }

                    $nextDay = date('Y-m-d', strtotime('+1 day', strtotime($nextDay)));
                }
            }else{
                if (!isset($days[$startDate])) {
                    $days[$startDate] = [$leave];
                } else {
                    $days[$startDate][] = $leave;
                }
            }
        }
        return $days;
    }

    /**
     * Récupère une permission
     * @param int $id
     * @return Leave
     * @throws \Exception
     */
    public function find (int $id): Leave {
        $statement = $this->pdo->query("SELECT * FROM leaves l ,events e WHERE e.id=l.id AND l.id = $id LIMIT 1");
        $result = $statement->fetch();
        $soldiers = new \Calendar\Soldiers($this->pdo);
        $soldier = $soldiers->find($result['refSoldier']);
        $leave = new \Calendar\Leave($result['name'], $result['description'], $result['start'], $result['end'], $result['type'], $result['state'], $result['location'], $result['phone'],$soldier);
        $leave->setId($result['id']);
        if ($result === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $leave;
    }

    /**
     * Récupère le nombre de visa pour la permission en param
     * @param Leave $leave
     * @return int
     * @throws \Exception
     */
    public function getNbVisa (Leave $leave): int {
        $id=$leave->getId();
        $statement = $this->pdo->query("SELECT COUNT(*) as nb FROM visa WHERE refLeave = $id LIMIT 1");
        $result = $statement->fetch();
        if ($result === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $result['nb'];
    }

    /**
     * @param Leave $leave
     * @param array $data
     * @return Leave
     */
    public function hydrate (Leave $leave, array $data) {
        $leave->setName($data['name']);
        $leave->setDescription($data['description']);
        $leave->setStart(\DateTimeImmutable::createFromFormat('Y-m-d H:i',
            $data['startDate'] . ' ' . $data['start'])->format('Y-m-d H:i:s'));
        $leave->setEnd(\DateTimeImmutable::createFromFormat('Y-m-d H:i',
            $data['endDate'] . ' ' . $data['end'])->format('Y-m-d H:i:s'));
        $leave->setType($data['type']);
        $leave->setState($data['state']);
        $leave->setLocation($data['location']);
        $leave->setPhone($data['phone']);
        $soldiers = new \Calendar\Soldiers($this->pdo);
        $soldier = $soldiers->find($data['soldier']);
        $leave->setSoldier($soldier);
        return $leave;
    }

    /**
     * Crée une permission au niveau de la base de donnée
     * @param Leave $leave
     * @return bool
     */
    public function create (Leave $leave): bool {
        $events = new \Calendar\Events($this->pdo);
        $event = new \Calendar\Event($leave->getName(),$leave->getDescription(),$leave->getStart()->format('Y-m-d H:i:s'),$leave->getEnd()->format('Y-m-d H:i:s'));
        $id = $events->create($event);
        $leave->setId($id); //créé un event, récupère son id et l'affecte à l'objet
        $statement = $this->pdo->prepare('INSERT INTO leaves (id,type, state, location, phone,refSoldier) VALUES (?, ?, ?, ?, ?, ?)');
        return $statement->execute([
           $leave->getId(),
           $leave->getType(),
           $leave->getState(),
           $leave->getLocation(),
           $leave->getPhone(),
           $leave->getSoldier()->getId(),
        ]);
    }

    /**
     * Met à jour une permission au niveau de la base de données
     * @param Leave $leave
     * @return bool
     */
    public function update (Leave $leave): bool {//on vérifie que l'état de la permision est différent de 'posee'
        $events = new \Calendar\Events($this->pdo);
        $event = new \Calendar\Event($leave->getName(),$leave->getDescription(),$leave->getStart()->format('Y-m-d H:i:s'),$leave->getEnd()->format('Y-m-d H:i:s'));
        $event->setId($leave->getId()); 
        $events->update($event);
        $statement = $this->pdo->prepare('UPDATE leaves SET type = ?, state = ?, location = ?, phone = ?, refSoldier=? WHERE id = ? and state="posee"' );
        return $statement->execute([
           $leave->getType(),
           $leave->getState(),
           $leave->getLocation(),
           $leave->getPhone(),
           $leave->getSoldier()->getId(),
           $leave->getId(),
        ]);
    }

    /**
     * TODO: Supprime une permission
     * @param int $idLeave
     * @return bool
     */
    public function delete (int $idLeave,int $idSoldier): bool {
        $statement = $this->pdo->query("SELECT * FROM leaves l ,events e WHERE e.id=l.id AND l.refSoldier = $idSoldier AND l.id=$idLeave and state='posee' LIMIT 1"); //on vérifie que la permision est bien à lui et que son état est différent de 'posee'
        $result = $statement->fetch();
        if ($result === false) {
            return false;
        }else{
            $count = $this->pdo->exec("DELETE FROM events WHERE id = $idLeave");
            if ($count === 1) {
                return true;
            }else{
                return false;
            }
        }
    }

    public function accept(int $idLeave,int $idSoldier): bool {
        $leave = $this->find($idLeave);
        $soldiers = new \Calendar\Soldiers($this->pdo);
        $soldier = $soldiers->find($idSoldier); //celui qui doit viser
        //on vérifie qu'il a le droit
        if($soldiers->canCheck($leave, $idSoldier)){
            $date = date('Y-m-d H:i:s');
            
            $statement = $this->pdo->prepare("INSERT INTO visa(refLeave, refSoldier,date,accepte,motifRefus) VALUES (?,?,?,1,'')");
            $statement->bindParam(1,$idLeave);
            $statement->bindParam(2,$idSoldier);
            $statement->bindParam(3,$date);
            
            $res = $statement->execute();
            // on change le statut de la permission
            if($this->getNbVisa($leave)==1){
                $this->pdo->exec("UPDATE leaves SET state ='visa' WHERE id =$idLeave");
            }else if($this->getNbVisa($leave)>=4){
                $this->pdo->exec("UPDATE leaves SET state ='validee' WHERE id =$idLeave");
            }
            if ($res === false) {
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
    
    public function reject(int $idLeave,int $idSoldier, string $motif): bool {
        $leave = $this->find($idLeave);
        $soldiers = new \Calendar\Soldiers($this->pdo);
        $soldier = $soldiers->find($idSoldier); //celui qui doit viser
        //on vérifie qu'il a le droit
        if($soldiers->canCheck($leave, $idSoldier)){
            $date = date('Y-m-d H:i:s');
            $statement = $this->pdo->prepare("INSERT INTO visa(refLeave, refSoldier,date,accepte,motifRefus) VALUES (?,?,?,0,?)");
            $statement->bindParam(1,$idLeave);
            $statement->bindParam(2,$idSoldier);
            $statement->bindParam(3,$date);
            $statement->bindParam(4,$motif);
            
            $res = $statement->execute();
            //on change le statut de la permission
            $this->pdo->exec("UPDATE leaves SET state ='refusee' WHERE id =$idLeave");
            
            if ($res === false) {
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

}
