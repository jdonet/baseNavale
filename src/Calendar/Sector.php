<?php
namespace Calendar;

class Sector{

    private $id;
    private $libelle;

    public function __construct($libelle =""){
        $this->libelle = $libelle;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getLibelle(): string {
        return $this->libelle;
    }

    public function setId (int $id) {
        $this->id = $id;
    }

    public function setLibelle (string $libelle) {
        $this->libelle = $libelle;
    }
}
