<?php
namespace Calendar;

class GuardTypes {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une section
     * @param int $id
     * @return GuardType
     * @throws \Exception
     */
    public function find (string $id): GuardType {
        $statement = $this->pdo->prepare("SELECT * FROM guardType WHERE guardType.id = ? LIMIT 1");
        $statement->bindParam(1,$id);
        $statement->execute();
        if($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $gt = new GuardType($row['name']);
            //positionnement de l'id
            $gt->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $gt;
    }

   
}
