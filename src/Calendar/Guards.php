<?php
namespace Calendar;

class Guards {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère les gardes commençant entre 2 dates
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return Guard[]
     */
    public function getGuardsBetween (\DateTimeInterface $start, \DateTimeInterface $end,int $idSoldier ): array {
        $sql = "SELECT * FROM guard g ,events e WHERE refSoldier =$idSoldier AND e.id=g.id AND (start BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}' OR end BETWEEN '{$start->format('Y-m-d 00:00:00')}' AND '{$end->format('Y-m-d 23:59:59')}') ORDER BY start ASC";
        $statement = $this->pdo->query($sql);
        $guards=[];
        while ($result = $statement->fetch()) {
            $guards[] = $this->find($result['id']);
        }
        return $guards;
    }

    /**
     * Récupère les gardes commençant entre 2 dates indexé par jour
     * @param \DateTimeInterface $start
     * @param \DateTimeInterface $end
     * @return array
     */
    public function getGuardsBetweenByDay (\DateTimeInterface $start, \DateTimeInterface $end,int $idSoldier): array {
        $guards = $this->getGuardsBetween($start, $end,$idSoldier);
        $days = [];
        foreach($guards as $guard) {
            $startDate = $guard->getStart()->format('Y-m-d');
            $endDate = $guard->getEnd()->format('Y-m-d');
            if($startDate != $endDate){
                $nextDay = $startDate;
                $nextEndDate = date('Y-m-d', strtotime('+1 day', strtotime($endDate)));
                while($nextDay!= $nextEndDate){
                    if (!isset($days[$nextDay])) {
                        $days[$nextDay] = [$guard];
                    } else {
                        $days[$nextDay][] = $guard;
                    }

                    $nextDay = date('Y-m-d', strtotime('+1 day', strtotime($nextDay)));
                }
            }else{
                if (!isset($days[$startDate])) {
                    $days[$startDate] = [$guard];
                } else {
                    $days[$startDate][] = $guard;
                }
            }
        }
        return $days;
    }

    /**
     * Récupère une garde
     * @param int $id
     * @return Guard
     * @throws \Exception
     */
    public function find (int $id): Guard {
        $statement = $this->pdo->query("SELECT * FROM guard g ,events e WHERE e.id=g.id AND g.id = $id LIMIT 1");
        $result = $statement->fetch();
        //récupération de la classe de l'étudiant
        $soldiers = new \Calendar\Soldiers($this->pdo);
        $soldier = $soldiers->find($result['refSoldier']);
        $guardtypes = new \Calendar\GuardTypes($this->pdo);
        $guardtype = $guardtypes->find($result['refGuardType']);
        $guard = new \Calendar\Guard($result['name'], $result['description'], $result['start'], $result['end'], $guardtype,$soldier);
        $guard->setId($result['id']);
        if ($result === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $guard;
    }

    

}
