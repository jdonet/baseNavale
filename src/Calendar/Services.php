<?php
namespace Calendar;

class Services {

    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Récupère une section
     * @param int $id
     * @return Service
     * @throws \Exception
     */
    public function find (int $id): Service {
        $statement = $this->pdo->query("SELECT * FROM service WHERE service.id = $id LIMIT 1");
        if($row = $statement->fetch()) {
            //appel du constructeur paramétré
            $service = new Service($row['name']);
            //positionnement de l'id
            $service->setId($row['id']);

        }
        if ($row === false) {
            throw new \Exception('Aucun résultat n\'a été trouvé');
        }
        return $service;
    }

   
}
