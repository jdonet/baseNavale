<?php
namespace Calendar;

class Guard extends Event{

    private $guardType;
    private $soldier;

    public function __construct($name ="", $description="", $start="", $end="", $guardType,$soldier){
        parent::__construct($name, $description, $start, $end);
        $this->guardType = $guardType;
        $this->soldier = $soldier;
    }

    public function getGuardType(): GuardType {
        return $this->guardType;
    }

    public function getSoldier(): Soldier {
        return $this->soldier;
    }

    public function setGuardType (string $guardType) {
        $this->guardType = $guardType;
    }

    public function setSoldier (string $soldier) {
        $this->soldier = $soldier;
    }
}
