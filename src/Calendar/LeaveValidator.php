<?php
namespace Calendar;

use App\Validator;

class LeaveValidator extends Validator {

    /**
     * @param array $data
     * @return array|bool
     */
    public function validates(array $data) {
        parent::validates($data);
        //$this->validate('type', 'minLength', 3);
        //$this->validate('type', 'maxLength', 3);
        return $this->errors;
    }

}
