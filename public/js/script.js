function deleteLeave(idLeave,token){
    if (window.confirm("Voulez-vous vraiment supprimer cette permission ?"))
        window.location.href = "calendar.php?delete="+idLeave+"&token="+token;
}
function acceptLeave(idLeave,token){
    if (window.confirm("Voulez-vous vraiment accepter cette permission ?"))
        window.location.href = "index.php?accept="+idLeave+"&token="+token;
}
function rejectLeave(idLeave,token){
    motif = document.getElementById("rejectCause").value;
    if (motif!=""){
        if (window.confirm("Voulez-vous vraiment refuser cette permission ?"))
            window.location.href = "index.php?reject="+idLeave+"&token="+token+"&motif="+motif;
    }else{
        alert("Merci d'indiquer un motif de refus");
        window.location.href = "editLeave.php?id="+idLeave;
    }
}