<?php
require '../src/bootstrap.php';
$pdo = get_pdo();
$leaves = new Calendar\Leaves($pdo);
if (!isset($_GET['id'])) {
    header('location: /404.php');
}
try {
    $leave = $leaves->find($_GET['id']);
} catch (\Exception $e) {
    e404();
}

render('header', ['title' => $leave->getName()]);
?>

<h1><?= h($leave->getEvent()->getName()); ?></h1>

<ul>
  <li>Date: <?= $leave->getEvent()->getStart()->format('d/m/Y'); ?></li>
  <li>Heure de démarrage: <?= $leave->getEvent()->getStart()->format('H:i'); ?></li>
  <li>Heure de fin: <?= $leave->getEvent()->getEnd()->format('H:i'); ?></li>
  <li>
    Description:<br>
      <?= h($leave->getEvent()->getDescription()); ?>
  </li>
</ul>

<?php require '../views/footer.php'; ?>
