<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../src/bootstrap.php';

if (isset($_SESSION['id'])){

    $pdo = get_pdo();
    $leavesManager = new Calendar\Leaves($pdo);
    $guardsManager = new Calendar\Guards($pdo);
    $soldiers = new Calendar\Soldiers($pdo);

//accepter la permission
if (isset($_GET['accept'])&& isset($_GET['token']) && $_GET['token']===$_SESSION['token']):
    $res = $leavesManager->accept($_GET['accept'], $_SESSION["id"]);
    if($res){
      header("Location: index.php?accepted=ok");	
    }else{
      header("Location: index.php?accepted=fail");	
    }
  endif;
//refuser la permission
if (isset($_GET['reject'])&& isset($_GET['token']) && isset($_GET['motif']) && $_GET['token']===$_SESSION['token']):
    $res = $leavesManager->reject($_GET['reject'], $_SESSION["id"],h($_GET['motif']));
    if($res){
      header("Location: index.php?rejected=ok");	
    }else{
      header("Location: index.php?rejected=fail");	
    }
  endif;


    $soldier = $soldiers->find($_SESSION['id']);
    $month = new Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null);
    $start = $month->getStartingDay();
    $start = $start->format('N') === '1' ? $start : $month->getStartingDay();
    $weeks = $month->getWeeks();
    $lastDay = $month->getLastDay();
    $end = $start->modify('+ 1 month -1 days');
    $leaves = $leavesManager->getLeavesBetweenByDay($start, $end,$_SESSION['id']);
    $allLeaves = $leavesManager->getAllLeavesToCheck(); //on récupère toutes les leaves
    $leavesToCheck = $soldiers->getLeavesToCheck($_SESSION['id'],$allLeaves); //on récupère les leaves à viser
    $textColors=getLeavesStates();

    require '../views/header.php';

    ?>

    <div class="calendar">
        <div class="row">
            <div class="col-9">
                <div class="row">
                    <div class="col-10">
                        <h1 class="text-center">
                            <a href="index.php?month=<?= $month->previousMonth()->month; ?>&year=<?= $month->previousMonth()->year; ?>" class="btn btn-info">&lt;</a>
                            <?= $month->toString(); ?>
                            <a href="index.php?month=<?= $month->nextMonth()->month; ?>&year=<?= $month->nextMonth()->year; ?>" class="btn btn-info">&gt;</a>
                        </h1>  
                    </div>
                    <div class="col-2">
                        <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseStats" aria-expanded="false" aria-controls="collapseStats">
                            Stats & Légende
                        </button>
                    </div>
                </div>
                    <div>
                        <div class="myPermissions">
                            <h2>Mes permissions </h2>
                            <table class="table">
                            <tbody>
                                <tr>
                                    <?php for ($i = 1; $i <= $lastDay; $i++):
                                        $date=$month->getDay($i);
                                        $leavesForDay = $leaves[$date->format('Y-m-d')] ?? [];
                                        if(empty($leavesForDay)){
                                            echo '<td>'. $i .'</td>';
                                        }else{
                                            foreach (getLeavesInfos() as $key => $value) {
                                                if($key === $leavesForDay[0]->getType())
                                                echo '<td class="'.$value[0].'"><div class="'.$textColors[$leavesForDay[0]->getState()][0].'">'. $i.'</div></td>';
                                            }
                                        }
                                    endfor; ?>
                                </tr>
                            </tbody>
                            </table>
                            
                        </div>
                        <div class="lowerPermissions">
                            <h2>Collegues</h2>
                            <table class="table">
                            <tbody>
                                <?php 
                                $allSoldiers = $soldiers->underOrders($soldier->getId());
                                foreach ($allSoldiers as $actualSoldier) :
                                ?>
                                    <tr>
                                        <td colspan="<?=$lastDay?>"><?=$actualSoldier->getFirstName()." ".$actualSoldier->getLastName()?> </td>
                                    </tr>
                                    <tr>
                                        <?php 
                                        $leavesActual = $leavesManager->getLeavesBetweenByDay($start, $end,$actualSoldier->getId());

                                        for ($i = 1; $i <= $lastDay; $i++):
                                            $date=$month->getDay($i);
                                            $leavesForDay = $leavesActual[$date->format('Y-m-d')] ?? [];
                                            if(empty($leavesForDay)){
                                                echo '<td>'. $i .'</td>';
                                            }else{
                                                foreach (getLeavesInfos() as $key => $value) {
                                                    if($key === $leavesForDay[0]->getType())
                                                    echo '<td class="'.$value[0].'"><div class="'.$textColors[$leavesForDay[0]->getState()][0].'">'. $i.'</div></td>';
                                                }
                                            }
                                        endfor; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            </table>
                        </td>
                        <div class="gradePermissions">
                            <h2>Gardes</h2>
                            <table class="table">
                            <tbody>
                                <?php 
                                $allSoldiers = $soldiers->onSameGuardType($soldier->getId());                                
                                foreach ($allSoldiers as $actualSoldier) :
                                ?>
                                    <tr>
                                        <td colspan="<?=$lastDay?>"><?=$actualSoldier->getFirstName()." ".$actualSoldier->getLastName()?> </td>
                                    </tr>
                                    <tr>
                                        <?php 
                                        $guardsActual = $guardsManager->getGuardsBetweenByDay($start, $end,$actualSoldier->getId());

                                        for ($i = 1; $i <= $lastDay; $i++):
                                            $date=$month->getDay($i);
                                            $guardsForDay = $guardsActual[$date->format('Y-m-d')] ?? [];
                                            if(empty($guardsForDay)){
                                                echo '<td>'. $i .'</td>';
                                            }else{
                                                echo '<td class="'.getLeavesInfos()["Garde"][0].'">'. $i.'</td>';
                                            }
                                        endfor; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>      
            </div>
            <div class="col-3 collapse stats" id="collapseStats">
                <div class="p-3 mb-2 bg-info text-white">
                    <h3>Statistiques de permission</h3>
                </div>
                <div>
                <h3>Légende</h3>
                <table class="table">
                    <tbody>
                        <tr><td>
                            <table class="table">
                                <tbody>
                                <tr><td>Texte</td></tr>
                                    <?php 
                                    foreach (getLeavesStates() as $key => $value) {
                                        echo '<tr><td class="'.$value[0].'">'.$key.'</td></tr>';
                                    }?>
                                </tbody>
                            </table>
                        </td><td>
                            <table class="table">
                                <tbody>
                                    <tr><td>Fond</td></tr>
                                    <?php 
                                    foreach (getLeavesInfos() as $key => $value) {
                                        
                                        echo '<tr><td class="'.$value[0].'">'.$value[1].'</td></tr>';
                                    }?>
                                </tbody>
                            </table>
                        </td></tr>
                    </tbody>
                </table>
                <div class="p-3 mb-2 bg-info text-white">
                <h3>Avancement des demandes</h3>
                </div>
                <div class="p-3 mb-2 bg-danger">
                    <h3><?= sizeof($leavesToCheck)?> demande(s) à viser</h3>
                    <ul class="list-group">
                    <?php
                        foreach ($leavesToCheck as $leave) {
                            echo '<li class="list-group-item"><a href="editLeave.php?id='.$leave->getId().'">'.$leave->getType().' : '.$leave->getSoldier()->getFirstName().' du '.$leave->getStart()->format('d M').' au '.$leave->getEnd()->format('d M').'</li>';
                        }
                    ?>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>

    <?php require '../views/footer.php'; 
}else{
  // pas connecté
  header("Location: login.php");

}