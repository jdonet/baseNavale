<?php
require '../src/bootstrap.php';

session_destroy();
header("Location: login.php");
?>