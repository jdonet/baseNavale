<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../src/bootstrap.php';


if (isset($_SESSION['id'])){

  $pdo = get_pdo();
  $leavesManager = new Calendar\Leaves($pdo);
  $month = new Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null);
  $start = $month->getStartingDay();
  $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
  $weeks = $month->getWeeks();
  $end = $start->modify('+' . (6 + 7 * ($weeks -1)) . ' days');
  $leaves = $leavesManager->getLeavesBetweenByDay($start, $end,$_SESSION['id']);

//suppression de la permission
  if (isset($_GET['delete'])&& isset($_GET['token']) && $_GET['token']===$_SESSION['token']):
    $res = $leavesManager->delete($_GET['delete'], $_SESSION["id"]);
    if($res){
      header("Location: calendar.php?deleted=ok");	
    }else{
      header("Location: calendar.php?deleted=fail");	
    }
  endif;

  require '../views/header.php';
  ?>

  <div class="calendar">

    <div class="d-flex flex-row align-items-center justify-content-between mx-sm-3">
      <h1><?= $month->toString(); ?></h1>

      <?php if (isset($_GET['success'])): ?>
        <div class="container">
          <div class="alert alert-success">
            L'évènement a bien été enregistré
          </div>
        </div>
      <?php endif; ?>
      <?php if (isset($_GET['deleted'])): ?>
        <div class="container">
        <?php if ($_GET['deleted']==="ok"): ?>
          <div class="alert alert-success">
            L'évènement a bien été supprimé
          <?php else: ?>
            <div class="alert alert-danger">
            Erreur lors de la suppression
          <?php endif; ?>    
          </div>
        </div>
      <?php endif; ?>

      <div>
        <a href="calendar.php?month=<?= $month->previousMonth()->month; ?>&year=<?= $month->previousMonth()->year; ?>" class="btn btn-primary">&lt;</a>
        <a href="calendar.php?month=<?= $month->nextMonth()->month; ?>&year=<?= $month->nextMonth()->year; ?>" class="btn btn-primary">&gt;</a>
      </div>
    </div>

    <table class="calendar__table calendar__table--<?= $weeks; ?>weeks">
        <?php for ($i = 0; $i < $weeks; $i++): ?>
          <tr>
              <?php
              foreach($month->days as $k => $day):
                  $date = $start->modify("+" . ($k + $i * 7) . " days");
                  $leavesForDay = $leaves[$date->format('Y-m-d')] ?? [];
                  $isToday = date('Y-m-d') === $date->format('Y-m-d');
                  ?>
                <td class="<?= $month->withinMonth($date) ? '' : 'calendar__othermonth'; ?> <?= $isToday ? 'is-today' : ''; ?>">
                    <?php if ($i === 0): ?>
                      <div class="calendar__weekday"><?= $day; ?></div>
                    <?php endif; ?>
                    <a tabindex="0"
                    class="calendar__day" 
                    role="button" 
                    data-trigger="focus" 
                    data-html="true" 
                    data-toggle="popover" 
                    data-placement="bottom" 
                    title="<b>Ajout</b>" 
                    data-content='<a href="addLeave.php?date=<?= $date->format('Y-m-d'); ?>">Permission</a><br>
                    <a href="addLeave.php">Garde</a><br>
                    <a href="addLeave.php">Récupération</a><br>'>
                    <?= $date->format('d'); ?></a>
                    <?php foreach($leavesForDay as $leave): ?>
                      <div class="calendar__event">
                          <a href="editLeave.php?id=<?= $leave->getId(); ?>"><?= h($leave->getType()); ?></a>
                      </div>
                    <?php endforeach; ?>
                </td>
              <?php endforeach; ?>
          </tr>
        <?php endfor; ?>
    </table>
<a tabindex="0"
   class="calendar__button" 
   role="button" 
   data-html="true" 
   data-trigger="focus" 
   data-toggle="popover" 
   data-placement="left" 
   title="<b>Ajout</b>" 
   data-content='<a href="addLeave.php">Permission</a><br>
   <a href="addLeave.php">Garde</a><br>
   <a href="addLeave.php">Récupération</a><br>'>+</a>    

  </div>

  <?php require '../views/footer.php'; 
}else{
  // pas connecté
  header("Location: login.php");

}