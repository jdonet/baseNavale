<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../src/bootstrap.php';

$pdo = get_pdo();

if (!empty($_POST)) {
	//professeur
	$soldiers = new Calendar\Soldiers($pdo);

	$login = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);

	if (!empty($login) AND !empty($password)) {
		$soldier = $soldiers->connect($login,$password);
		if($soldier != false) {
			$_SESSION['id']=$soldier->getId();
			$_SESSION['pseudo']=$soldier->getLogin();
			//génération d'un code aléatoire de session
			$_SESSION['token'] = md5(time()*rand(45,234));

			header("Location: index.php");	
		}else{
			$erreur="Mauvais pseudo ou mot de passe";
		}
	}
	else{
		$erreur="Tout les champs doivent etre complétés";
	}
	}

require '../views/header.php';
?>

<div class="calendar">
	<div class="container">
		<div class="row">
			<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
			<form action="" method="POST" class="col-xs-12 col-sm-12 col-md-9 col-lg-9">

			<img class="avatar" src="./img/avatar.png">

			<h1>Se connecter</h1>
			<hr>

			<div class="form-group">
				<label for="username">Pseudo</label>
				<input type="text" name="username" class="form-control" placeholder="Pseudo" value="julien">
			</div>
			<br>

			<div class="form-group">
				<label for="password">Mot de passe</label>
				<input type="password" name="password" class="form-control" placeholder="Mot de passe" value="donet">
			</div>
			<br>

			<?php
			if (isset($erreur)) {
				echo "<p style='color:red;'>".$erreur."</p><br>";
			}
			?>

			<button type="submit" class="btn btn-primary">Se connecter</button>

		</form>
		<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>

		</div>

	</div>

</div>

<?php require '../views/footer.php'; ?>
