<?php
require '../src/bootstrap.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['id'])){
    $pdo = get_pdo();
    $leaves = new Calendar\Leaves($pdo);
    $soldiersManager = new Calendar\Soldiers($pdo);

    $errors = [];
    try {
        $leave = $leaves->find($_GET['id'] ?? null);
    } catch (\Exception $e) {
        e404();
    } catch (\Error $e) {
        e404();
    }

    $data = [
        'name'        => $leave->getName(),
        'startDate'        => $leave->getStart()->format('Y-m-d'),
        'start'       => $leave->getStart()->format('H:i'),
        'endDate'        => $leave->getEnd()->format('Y-m-d'),
        'end'         => $leave->getEnd()->format('H:i'),
        'description' => $leave->getDescription(),
        'type' => $leave->getType(),
        'state' => $leave->getState(),
        'location' => $leave->getLocation(),
        'phone' => $leave->getPhone()
    ];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = $_POST;
        $validator = new Calendar\LeaveValidator();
        $errors = $validator->validates($data);
        if (empty($errors)) {
            $leaves->hydrate($leave, $data);
            $leaves->update($leave);
            header('Location: calendar.php?success=1&month='. $leave->getStart()->format('m').'&year='.$leave->getStart()->format('Y'));
            exit();
        }
    }

    render('header', ['title' => $leave->getName()]);
    ?>

    <div class="container">

    <h1>Editer la permission
        <small><?= h($leave->getName()); ?></small>
    </h1>
    <?php 
        $visa="";
        if($soldiersManager->canCheck($leave, $_SESSION['id'])): //s'il est autorisé à valider
            $visa = '
            <button type="button" class="btn btn-success" onclick="acceptLeave('.h($leave->getId()).', \''. $_SESSION['token'].'\')">Accepter</button> 
            <button type="button" class="btn btn-warning"onclick="rejectLeave('.h($leave->getId()).', \''.$_SESSION['token'].'\')">Refuser</button>
            <input type="text" class="form-control inline" name="rejectCause" id="rejectCause" placeholder="motif du refus"> 
            ';
        endif;
        
        if($leave->getState()==="posee"): ?>
            <form action="editLeave.php?id=<?= $_GET['id']?>" method="post">
            <?php render('calendar/formLeave', ['data' => $data, 'errors' => $errors]); ?>
            <div class="form-group">
            <button class="btn btn-primary">Modifier la permission</button>
            <button type="button" class="btn btn-danger" onclick="deleteLeave(<?= h($leave->getId()); ?>, '<?= $_SESSION['token']; ?>')">Supprimer la permission</button>
            <?= $visa; ?>
            </div>
            </form>
    <?php  else :
            render('calendar/formLeave', ['data' => $data, 'errors' => $errors]);?>
              <div class="form-group">
            <a class="btn btn-primary" href="calendar.php" role="button">Retour</a>
            <?= $visa; ?>
            </div>
            
    <?php endif;?>
    </div>

    <?php render('footer'); 

}else{
    // pas connecté
    header("Location: login.php");
  
 }
