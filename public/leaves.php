<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require '../src/bootstrap.php';

if (isset($_SESSION['id'])){

  $pdo = get_pdo();
  $leaves = new Calendar\Leaves($pdo);
  $month = new Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null);
  $start = $month->getStartingDay();
  $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
  $weeks = $month->getWeeks();
  $end = $start->modify('+' . (6 + 7 * ($weeks -1)) . ' days');
  $leaves = $leaves->getLeavesBetweenByDay($start, $end,$_SESSION['id']);
  require '../views/header.php';
  ?>

  <div class="calendar">

    Liste des permissions

  </div>

  <?php require '../views/footer.php'; 


}else{
  // pas connecté
  header("Location: login.php");

}