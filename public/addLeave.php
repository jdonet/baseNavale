<?php
require '../src/bootstrap.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (isset($_SESSION['id'])){

  $data = [
      'startDate'  => $_GET['date'] ?? date('Y-m-d'),
      'endDate'  => $_GET['date'] ?? date('Y-m-d'),
      'start' => date('H:i'),
      'end'   => date('H:i')
  ];
  $validator = new \App\Validator($data);
  if (!$validator->validate('date', 'date')) {
      $data['date'] = date('Y-m-d');
  }
  $errors = [];
  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $data = $_POST;
      $validator = new Calendar\LeaveValidator();
      $errors = $validator->validates($_POST);
      if (empty($errors)) {
          $leaves = new \Calendar\Leaves(get_pdo());
          $leave = $leaves->hydrate(new \Calendar\Leave(), $data);
          $leaves->create($leave);
          header("Location: calendar.php?success=1&month=".$leave->getStart()->format('m')."&year=".$leave->getStart()->format('Y'));
          exit();
      }
  }

  render('header', ['title' => 'Ajouter une permission']);
  ?>

  <div class="container">

      <?php if (!empty($errors)): ?>
        <div class="alert alert-danger">
          Merci de corriger vos erreurs
        </div>
      <?php endif; ?>

    <h1>Ajouter une permission</h1>
    <form action="addLeave.php?month=<?= $_GET['month']?>&year=<?= $_GET['year']?>" method="post" class="form">
        <?php render('calendar/formLeave', ['data' => $data, 'errors' => $errors]); ?>
      <div class="form-group">
        <button class="btn btn-primary">Ajouter la permission</button>
      </div>
    </form>
  </div>
  <?php render('footer');

}else{
  // pas connecté
  header("Location: login.php");

}